<?php

namespace Drupal\geofield_directions\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\geofield\Plugin\Field\FieldFormatter\GeofieldDefaultFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "geofield_directions",
 *   label = @Translation("Geofield Directions"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class GeofieldDirectionsFormatter extends GeofieldDefaultFormatter {

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  private Token $token;

  /**
   * The EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * GeofieldDirectionsFormatter constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\geofield\GeoPHP\GeoPHPInterface $geophp_wrapper
   *   The geoPhpWrapper.
   * @param \Drupal\Core\Utility\Token $token
   *   The token replacement service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity-type manager service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    GeoPHPInterface $geophp_wrapper,
    Token $token,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $geophp_wrapper);
    $this->token = $token;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('geofield.geophp'),
      $container->get('token'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $defaults = [
      'link_text' => 'Get directions',
      'new_tab' => TRUE,
      'html_attributes' => '',
      // Default zoom used by Google Maps, provides more comfortable
      // results since displays street names and places.
      'zoom' => 17,
    ];
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#description' => $this->t('This field supports tokens.'),
      '#default_value' => $this->getSetting('link_text'),
      '#token_types' => [$this->getTokenType()],
      '#element_validate' => ['token_element_validate'],
      '#after_build' => ['token_element_validate'],
      '#required' => TRUE,
    ];
    $form['html_attributes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('HTML attributes'),
      '#description' => $this->t('Add additional HTML attributes using <em>attribute=value</em> format, separate multiple attributes by semicolons (;). The <strong>href</strong> and <strong>target</strong> attributes are excluded. This field supports tokens.'),
      '#default_value' => $this->getSetting('html_attributes'),
      '#token_types' => [$this->getTokenType()],
      '#element_validate' => ['token_element_validate'],
      '#after_build' => ['token_element_validate'],
    ];
    // Show the token help relevant to this pattern type.
    $form['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [$this->getTokenType()],
    ];
    $form['new_tab'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open link in a new tab'),
      '#default_value' => $this->getSetting('new_tab'),
    ];
    $form['zoom'] = [
      '#type' => 'number',
      '#title' => $this->t('Zoom'),
      '#min' => 1,
      '#max' => 22,
      '#step' => 0.25,
      '#default_value' => $this->getSetting('zoom'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary[] = $this->t('Link text: @text',
      [
        '@text' => $this->getSetting('link_text'),
      ]
    );
    $boolean = !empty($this->getSetting('html_attributes'))
      ? $this->t('Yes')
      : $this->t('No');
    $summary[] = $this->t('Additional HTML attributes: @bool', [
      '@bool' => $boolean,
    ]);
    $boolean = !empty($this->getSetting('new_tab'))
      ? $this->t('Yes')
      : $this->t('No');
    $summary[] = $this->t('Open in new tab: @bool', [
      '@bool' => $boolean,
    ]);
    $summary[] = $this->t('Zoom: @zoom',
      [
        '@zoom' => $this->getSetting('zoom'),
      ]
    );
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();
    $token_data = [$this->getTokenType() => $entity];
    foreach ($items as $delta => $item) {
      $geom = $this->geoPhpWrapper->load($item->value);
      if ($geom && $geom->getGeomType() == 'Point') {
        $lat = $geom->y();
        $lon = $geom->x();
        $zoom = $this->getSetting('zoom');
        $url = Url::fromUri('https://maps.google.com/maps', [
          'query' => [
            'q' => "$lat,$lon",
            'z' => $zoom,
            'll' => "$lat,$lon",
          ],
        ]);
        $link_text = $this->getSetting('link_text');
        $link_text = $this->token->replace($link_text, $token_data, ['clear' => TRUE]);
        $link = Link::fromTextAndUrl(Html::decodeEntities($link_text), $url);
        $build = $link->toRenderable();

        if (empty($build['#attributes'])) {
          $build['#attributes'] = [];
        }

        $html_attributes = $this->getSetting('html_attributes');
        $html_attributes = self::parseHtmlAttributes($html_attributes);
        if (!empty($html_attributes['class'])) {
          $html_attributes['class'] = explode(' ', $html_attributes['class']);
        }
        array_walk_recursive(
          $html_attributes,
          [$this, 'replaceTokens'],
          $token_data
        );
        if (!empty($html_attributes['class'])) {
          foreach ($html_attributes['class'] as &$item) {
            $item = Html::getClass($item);
          }
        }
        $build['#attributes'] += $html_attributes;
        $build['#attributes']['target'] = !empty($this->getSetting('new_tab')) ? '_blank' : '_self';

        $element[$delta] = $build;
      }
    }

    return $element;
  }

  /**
   * Array Recursive Walk callback fro replacing tokens.
   *
   * @param mixed $attribute
   *   Value passed in by array_walk_recursive.
   * @param mixed $key
   *   Key passed in by array_walk_recursive.
   * @param array $token_data
   *   Token data.
   */
  private function replaceTokens(&$attribute, $key, $token_data) {
    $attribute = Html::decodeEntities($this->token->replace($attribute, $token_data, ['clear' => TRUE]));
  }

  /**
   * Parse HTML attributes.
   *
   * Attributes use the format attribute1=valor;attribute.
   *
   * @param string $html_attributes
   *   The HTML attributes in string format.
   *
   * @return array
   *   The parsed HTML attributes.
   */
  public static function parseHtmlAttributes(string $html_attributes): array {
    $html_attributes = trim($html_attributes, "; \t\n\r\0\x0B");

    if (empty($html_attributes)) {
      return [];
    }

    $attributes = [];
    foreach (explode(';', $html_attributes) as $item) {
      [$key, $value] = explode('=', trim($item));
      // Class attributes should be passed as an array.
      $attributes[$key] = $value;
    }

    $attributes = array_filter($attributes);
    unset($attributes['href'], $attributes['target']);

    return $attributes;
  }

  /**
   * Get the token type for the parent entity-type.
   *
   * @return mixed|null
   *   The potential token type.
   */
  private function getTokenType() {
    $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    $entities = $this->entityTypeManager->getDefinitions();

    return !empty($entities[$entity_type]) ? $entities[$entity_type]->get('token_type') : NULL;
  }

}
