<?php

namespace Drupal\Tests\geofield_directions\Kernel;

use Drupal\Component\Utility\Html;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Test case for 'geofield_directions' formatter.
 *
 * @group geofield_directions
 * @coversDefaultClass \Drupal\geofield_directions\Plugin\Field\FieldFormatter\GeofieldDirectionsFormatter
 */
class GeofieldDirectionsFormatterTest extends EntityKernelTestBase {

  /*
   * Earth radius in miles.
   */
  protected const EARTH_RADIUS = 3959;

  protected const MAX_LAT = 90;

  protected const MAX_LON = 180;

  /**
   * The entity type used in this test.
   *
   * @var string
   */
  protected $entityType = 'node';

  /**
   * The bundle used in this test.
   *
   * @var string
   */
  protected $bundle = 'article';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'filter', 'geofield', 'token', 'geofield_directions'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['filter', 'node', 'system']);

    $node_type = NodeType::create(['type' => 'article', 'name' => 'Article']);
    $node_type->save();

    FieldStorageConfig::create([
      'field_name' => 'geofield',
      'entity_type' => $this->entityType,
      'type' => 'geofield',
      'settings' => [
        'backend' => 'geofield_backend_default',
      ],
    ]
    )->save();

    FieldConfig::create([
      'entity_type' => $this->entityType,
      'bundle' => $this->bundle,
      'field_name' => 'geofield',
      'label' => 'GeoField',
      'settings' => [
        'backend' => 'geofield_backend_default',
      ],
    ])->save();
  }

  /**
   * Tests geofield field directions formatter.
   *
   * @covers ::viewElements
   * @dataProvider coordinatesProvider
   */
  public function testFormatter($point, $settings, $coordinates, $attributes, $title, $expected_attributes) {
    $account = $this->createUser();
    // Create the entity to be referenced.
    $entity = Node::create([
      'title' => $title,
      'type' => $this->bundle,
      'tnid' => 0,
      'uid' => $account->id(),
    ]);
    $entity->geofield = [
      'value' => $point,
    ];
    $entity->save();

    // Verify the geofield field formatter's render array.
    $build = $entity->get('geofield')->view(['type' => 'geofield_directions', 'settings' => $settings]);
    [$lat, $lon] = $coordinates;
    $url = Url::fromUri('https://maps.google.com/maps', [
      'query' => [
        'q' => "$lat,$lon",
        'z' => $settings['zoom'],
        'll' => "$lat,$lon",
      ],
    ]);
    $token = \Drupal::token();
    $link_text = $token->replace($settings['link_text'], [$this->entityType => $entity], ['clear' => TRUE]);
    $link = Link::fromTextAndUrl(Html::decodeEntities($link_text), $url);
    // Store the callback to allow recursive calls.
    $expected_value = $link->toRenderable();
    $expected_value['#attributes'] = $expected_attributes;
    \Drupal::service('renderer')->renderRoot($expected_value);
    \Drupal::service('renderer')->renderRoot($build[0]);
    $this->assertEquals($expected_value['#markup'], $build[0]['#markup']);
  }

  /**
   * Provides test data for testFormatter().
   */
  public function coordinatesProvider(): array {
    $data = [];
    for ($i = 0; $i < 9; $i++) {
      // Getting rid of random names as that can cause issues that can't be
      // reproduced.
      if ($i == 0) {
        $title = 'Node with HTML Entities &"<>\'©;';
      }
      else {
        $title = 'Node ' . $i;
      }
      $centre_lat = rand(-1 * self::MAX_LAT, self::MAX_LAT);
      $centre_lon = rand(-1 * self::MAX_LON, self::MAX_LON);
      [$lat, $lon] = $this->generateCoordinates([$centre_lat, $centre_lon], rand(1, self::EARTH_RADIUS - 1));
      // Test for tokens in the link text.
      $link_text = "Go to [{$this->entityType}:title]";
      $zoom = rand(14, 20);
      $settings = [
        'zoom' => $zoom,
        'link_text' => $link_text,
        // Test allow link open a new tab.
        'new_tab' => (bool) rand(0, 1),
        // Test for HTML attributes, include spaces, duplicates href and
        // target attributes, set custom attribute.
        'html_attributes' => "class=link link--[{$this->entityType}:title]; href=/go/nowhere; target=_self; data-x-attribute=value",
      ];
      $attributes = [
        'class' => [
          'link',
          "link--[{$this->entityType}:title]",
        ],
        'data-x-attribute' => 'value',
        'target' => $settings['new_tab'] ? '_blank' : '_self',
      ];
      $expected_attributes = $attributes;
      $expected_attributes['class'] = [
        'link',
        "link--" . Html::getClass($link_text),
      ];
      $data[] = [
        "POINT ($lon $lat)",
        $settings,
        [$lat, $lon],
        $attributes,
        $title,
        $expected_attributes,
      ];
    }

    return $data;
  }

  /**
   * Generates a random set of coordinates.
   *
   * Given a $centre (latitude, longitude) co-ordinates and a
   * distance $radius (miles), returns a random point (latitude, longitude)
   * which is within $radius miles of $centre.
   *
   * @param array $centre
   *   Numeric array of floats. First element is
   *   latitude, second is longitude.
   * @param float $radius
   *   The radius (in miles).
   *
   * @return array         Numeric array of floats (lat/lng). First
   *   element is latitude, second is longitude.
   *
   * @link https://stackoverflow.com/a/21646258
   */
  protected function generateCoordinates($centre, $radius) {
    // Pick random distance within $distance;.
    $distance = lcg_value() * $radius;
    // Convert degrees to radians.
    $centre_rads = array_map('deg2rad', $centre);
    // First suppose our point is the north pole.
    // Find a random point $distance miles away.
    $lat_rads = (pi() / 2) - $distance / self::EARTH_RADIUS;
    $lng_rads = lcg_value() * 2 * pi();
    // ($lat_rads,$lng_rads) is a point on the circle which is
    // distance in miles from the North Pole. Convert to Cartesian.
    $x1 = cos($lat_rads) * sin($lng_rads);
    $y1 = cos($lat_rads) * cos($lng_rads);
    $z1 = sin($lat_rads);
    // Rotate that sphere so that the north pole is now at $centre.
    // Rotate in x axis by $rot = (pi()/2) - $centre_rads[0];.
    $rot = (pi() / 2) - $centre_rads[0];
    $x2 = $x1;
    $y2 = $y1 * cos($rot) + $z1 * sin($rot);
    $z2 = -$y1 * sin($rot) + $z1 * cos($rot);
    // Rotate in z axis by $rot = $centre_rads[1].
    $rot = $centre_rads[1];
    $x3 = $x2 * cos($rot) + $y2 * sin($rot);
    $y3 = -$x2 * sin($rot) + $y2 * cos($rot);
    $z3 = $z2;
    // Finally, convert this point to polar coords.
    $lng_rads = atan2($x3, $y3);
    $lat_rads = asin($z3);

    return array_map('rad2deg', [$lat_rads, $lng_rads]);
  }

}
